package com.orquest.dummytest.model.entity;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "WORKLOG", schema = "WLOG")
public class Worklog {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "BUSINESS_ID")
	private Integer businessId;

	@Column(name = "DATE")
	private Instant date;

	@Column(name = "EMPLOYEE_ID")
	private Integer employeeId;

	@JoinColumn(name = "RECORD_ID")
	@ManyToOne(fetch = FetchType.EAGER)
	private WorklogRecordType recordType;

	@JoinColumn(name = "SERVICE_ID")
	@ManyToOne(fetch = FetchType.EAGER)
	private ServiceType serviceId;

	@JoinColumn(name = "TYPE_ID")
	@ManyToOne(fetch = FetchType.EAGER)
	private WorklogWorkType type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Integer businessId) {
		this.businessId = businessId;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public WorklogRecordType getRecordType() {
		return recordType;
	}

	public void setRecordType(WorklogRecordType recordType) {
		this.recordType = recordType;
	}

	public ServiceType getServiceId() {
		return serviceId;
	}

	public void setServiceId(ServiceType serviceId) {
		this.serviceId = serviceId;
	}

	public WorklogWorkType getType() {
		return type;
	}

	public void setType(WorklogWorkType type) {
		this.type = type;
	}

}
