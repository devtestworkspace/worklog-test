package com.orquest.dummytest.model.dto;

public class WorklogDtoRaw extends WorklogDto {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "WorklogDtoRaw [id=" + id + "] " + super.toString();
	}
	
	

}
