package com.orquest.dummytest.model.dto;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import com.orquest.dummytest.model.entity.Worklog;

public class ValidationDto {

	private ZonedDateTime date;
	private List<Worklog> worklogs = new ArrayList<Worklog>();
	private Duration duration;
	private String alert;
	
	public ZonedDateTime getDate() {
		return date;
	}
	public void setDate(ZonedDateTime date) {
		this.date = date;
	}
	public List<Worklog> getWorklogs() {
		return worklogs;
	}
	public void setWorklogs(List<Worklog> worklogs) {
		this.worklogs = worklogs;
	}
	public Duration getDuration() {
		return duration;
	}
	public void setDuration(Duration duration) {
		this.duration = duration;
	}
	public String getAlert() {
		return alert;
	}
	public void setAlert(String alert) {
		this.alert = alert;
	}

	public void addWorklog(Worklog worklog) {
		worklogs.add(worklog);
	}
	public void addAlert(String alert) {
		if (this.alert == null) {
			this.alert = alert;
		} else {
			this.alert += " | " + alert;
		}
	}
	@Override
	public String toString() {
		return "ValidationDto [date=" + date + ", worklogs=" + worklogs + ", duration=" + duration + ", alert=" + alert
				+ "]";
	}
}
