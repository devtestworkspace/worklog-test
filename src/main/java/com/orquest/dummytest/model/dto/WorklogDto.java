package com.orquest.dummytest.model.dto;

public class WorklogDto {

	private String businessId;
	private String date;
	private String employeeId;
	private String recordType;
	private String serviceId;
	private String type;
	

	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "WorklogDto [businessId=" + businessId + ", date=" + date + ", employeeId=" + employeeId
				+ ", recordType=" + recordType + ", serviceId=" + serviceId + ", type=" + type + "]";
	}

}
