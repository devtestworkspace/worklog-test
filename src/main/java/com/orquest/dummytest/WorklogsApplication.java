package com.orquest.dummytest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorklogsApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorklogsApplication.class, args);
	}

}
