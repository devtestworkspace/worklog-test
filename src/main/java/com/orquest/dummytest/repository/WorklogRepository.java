package com.orquest.dummytest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orquest.dummytest.model.entity.Worklog;

public interface WorklogRepository extends JpaRepository<Worklog, Long>, WorklogRepositoryCustom {
  

	List<Worklog> findByEmployeeIdOrderByDate(Integer employeeId);

}
