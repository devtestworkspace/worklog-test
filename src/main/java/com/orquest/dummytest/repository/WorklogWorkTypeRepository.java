package com.orquest.dummytest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orquest.dummytest.model.entity.WorklogWorkType;

public interface WorklogWorkTypeRepository extends JpaRepository<WorklogWorkType, Integer> {
  

	WorklogWorkType findFirstByName(String name);

}
