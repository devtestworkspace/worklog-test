package com.orquest.dummytest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orquest.dummytest.model.entity.WorklogRecordType;

public interface WorklogRecordTypeRepository extends JpaRepository<WorklogRecordType, Integer> {
  

	WorklogRecordType findFirstByName(String name);

}
