package com.orquest.dummytest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orquest.dummytest.model.entity.ServiceType;

public interface ServiceTypeRepository extends JpaRepository<ServiceType, Integer> {
  

	ServiceType findFirstByName(String name);

}
