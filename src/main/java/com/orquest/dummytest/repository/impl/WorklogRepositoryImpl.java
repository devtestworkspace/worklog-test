package com.orquest.dummytest.repository.impl;

import java.time.Instant;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.orquest.dummytest.model.entity.Worklog;
import com.orquest.dummytest.repository.WorklogRepositoryCustom;

public class WorklogRepositoryImpl implements WorklogRepositoryCustom {
  
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Worklog> findByEmployeeIdBetweenDates(Integer employeeId, Instant fromDate, Instant toDate) {
		StringBuilder sqlString = new StringBuilder("SELECT * " + 
				" FROM WLOG.WORKLOG " + 
				" WHERE EMPLOYEE_ID = :employeeId ");
		if (fromDate != null) {
			sqlString.append(" AND date >= :fromDate");
		}
		if (toDate != null) {
			sqlString.append(" AND date <= :toDate");
		}
		sqlString.append(" ORDER BY date");
		
		Query sqlquery = em.createNativeQuery(sqlString.toString(), Worklog.class);
		sqlquery.setParameter("employeeId", employeeId);
		if (fromDate != null) {
			sqlquery.setParameter("fromDate", fromDate);
		}
		if (toDate != null) {
			sqlquery.setParameter("toDate", toDate);
		}
		
		return sqlquery.getResultList();
	}

}
