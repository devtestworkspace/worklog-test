package com.orquest.dummytest.repository;

import java.time.Instant;
import java.util.List;

import com.orquest.dummytest.model.entity.Worklog;

public interface WorklogRepositoryCustom {
  
	List<Worklog> findByEmployeeIdBetweenDates(Integer employeeId, Instant fromDate, Instant toDate);

}
