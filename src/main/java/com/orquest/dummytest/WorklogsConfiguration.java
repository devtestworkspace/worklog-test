package com.orquest.dummytest;

import java.time.Instant;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.orquest.dummytest.model.dto.WorklogDto;
import com.orquest.dummytest.model.dto.WorklogDtoRaw;
import com.orquest.dummytest.model.entity.ServiceType;
import com.orquest.dummytest.model.entity.Worklog;
import com.orquest.dummytest.model.entity.WorklogRecordType;
import com.orquest.dummytest.model.entity.WorklogWorkType;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class WorklogsConfiguration {

	@Bean
	@Scope("prototype")
	public Logger produceLogger(InjectionPoint injectionPoint) {
		Class<?> classOnWired = injectionPoint.getMember().getDeclaringClass();
		return LoggerFactory.getLogger(classOnWired);
	}
	
	@Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
          .select()
          .apis(RequestHandlerSelectors.any())
          .paths(PathSelectors.any())
          .build();
    }

	@Bean
	public ModelMapper modelMapper() {

		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		Converter<Instant, String> formatInstantToString = ctx -> ctx.getSource() != null ? ctx.getSource().toString()
				: "";
		Converter<String, Instant> formatStringToInstant = ctx -> ctx.getSource() != null
				? Instant.parse(ctx.getSource())
				: null;

		Converter<WorklogRecordType, String> formatRecordTypeToString = ctx -> {
			WorklogRecordType src = ctx.getSource();
			if (src == null) {
				return null;
			}
			return src.getName();
		};
		Converter<ServiceType, String> formatServiceIdToString = ctx -> {
			ServiceType src = ctx.getSource();
			if (src == null) {
				return null;
			}
			return src.getName();
		};
		
		Converter<WorklogWorkType, String> formatWorkTypeToString = ctx -> ctx.getSource() != null ? ctx.getSource().getName()
				: "";

		modelMapper.createTypeMap(WorklogDto.class, Worklog.class);
		modelMapper.createTypeMap(WorklogDtoRaw.class, Worklog.class);
		modelMapper.createTypeMap(Worklog.class, WorklogDto.class);

		modelMapper.typeMap(Worklog.class, WorklogDto.class).addMappings(mapper -> {

			mapper.using(formatInstantToString).map(Worklog::getDate, WorklogDto::setDate);
			mapper.using(formatRecordTypeToString).map(Worklog::getRecordType, WorklogDto::setRecordType);
			mapper.using(formatServiceIdToString).map(Worklog::getServiceId, WorklogDto::setServiceId);
			mapper.using(formatWorkTypeToString).map(Worklog::getType, WorklogDto::setType);

		});
		modelMapper.typeMap(WorklogDto.class, Worklog.class).addMappings(mapper -> {

			mapper.using(formatStringToInstant).map(WorklogDto::getDate, Worklog::setDate);
		});
		

		return modelMapper;
	}

}
