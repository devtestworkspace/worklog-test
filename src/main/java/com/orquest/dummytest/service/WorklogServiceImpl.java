package com.orquest.dummytest.service;

import java.lang.reflect.InvocationTargetException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orquest.dummytest.model.dto.ValidationDto;
import com.orquest.dummytest.model.dto.WorklogDto;
import com.orquest.dummytest.model.entity.ServiceType;
import com.orquest.dummytest.model.entity.Worklog;
import com.orquest.dummytest.model.entity.WorklogRecordType;
import com.orquest.dummytest.model.entity.WorklogWorkType;
import com.orquest.dummytest.repository.ServiceTypeRepository;
import com.orquest.dummytest.repository.WorklogRecordTypeRepository;
import com.orquest.dummytest.repository.WorklogRepository;
import com.orquest.dummytest.repository.WorklogWorkTypeRepository;

@Service
public class WorklogServiceImpl implements WorklogService {

	
	public static final String IN = "IN";
	public static final String OUT = "OUT";
	public static final String WORK = "WORK";
	public static final String REST = "REST";
	
	public static final String VOID_WORKLOG = "Existen registros sin fecha";
	public static final String MISSING_FIRST_START = "Falta el primer registro de inicio de trabajo (WORK, IN)";
	public static final String MISSING_WORK = "Falta un registro de inicio de trabajo (WORK, IN)";
	public static final String MISSING_REST = "Falta un registro de fin de descanso (REST, OUT)";
	public static final String MISSING_END = "Falta el registro de fin de trabajo (WORK, OUT)";

	@Autowired
	WorklogRepository worklogRepository;

	@Autowired
	WorklogWorkTypeRepository workTypeRepository;

	@Autowired
	WorklogRecordTypeRepository recordTypeRepository;

	@Autowired
	ServiceTypeRepository serviceTypeRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	protected Logger logger;

	@Override
	public List<WorklogDto> findByFilter(WorklogDto filter) {

		Worklog probe = modelMapper.map(filter, Worklog.class);

		Sort sortBy = Sort.by("date");
		worklogRepository.findAll(Example.of(probe), sortBy);

		List<WorklogDto> response = new ArrayList<WorklogDto>();
		logger.info("getAllCargos " + filter);

		worklogRepository.findAll(Example.of(probe)).forEach(worklog -> {
			response.add(modelMapper.map(worklog, WorklogDto.class));
		});
		return response;
	}

	@Override
	public List<WorklogDto> findByEmployeeId(Integer employeeId) {
		List<WorklogDto> result = new ArrayList<WorklogDto>();
		worklogRepository.findByEmployeeIdOrderByDate(employeeId).forEach(ent -> {
			result.add(modelMapper.map(ent, WorklogDto.class));
		});
		return result;
	}

	@Override
	public List<WorklogDto> create(List<WorklogDto> worklogDto)
			throws IllegalAccessException, InvocationTargetException {

		List<WorklogDto> response = new ArrayList<WorklogDto>();
		worklogDto.forEach(dto -> {
			try {
				response.add(create(dto));
			} catch (Exception e) {
				logger.error("Error creating worklog " + dto, e);
			}
		});
		return response;
	}

	@Transactional
	public WorklogDto create(WorklogDto worklogDto) {

		Worklog worklog = dtoToPersistenceCtx(worklogDto);

		return modelMapper.map(worklogRepository.save(worklog), WorklogDto.class);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		worklogRepository.deleteById(id);
	}

	@Override
	public void update(Long id, WorklogDto updated) {
		Optional<Worklog> entity = worklogRepository.findById(id);
		if (!entity.isPresent()) {
			return;
		}
		Worklog worklog = entity.get();
		worklog = dtoToPersistenceCtx(updated, worklog);
		
		worklogRepository.save(worklog);
		
	}
	

	private Worklog dtoToPersistenceCtx(WorklogDto worklogDto) {
		return dtoToPersistenceCtx(worklogDto, new Worklog());
	}
	
	private Worklog dtoToPersistenceCtx(WorklogDto worklogDto, Worklog worklog) {

		modelMapper.map(worklogDto, worklog);
		worklog.setRecordType(findRecordTypeByName(worklogDto.getRecordType()));
		worklog.setType(findWorkTypeByName(worklogDto.getType()));
		worklog.setServiceId(findServiceByName(worklogDto.getServiceId()));
		return worklog;
	}
	
	public WorklogRecordType findRecordTypeByName(String name) {
		WorklogRecordType recordType = recordTypeRepository.findFirstByName(name);
		if (recordType == null) {
			recordType = new WorklogRecordType();
			recordType.setName(name);
			recordType = recordTypeRepository.save(recordType);
		}
		return recordType;
	}

	public WorklogWorkType findWorkTypeByName(String name) {
		WorklogWorkType workType = workTypeRepository.findFirstByName(name);
		if (workType == null) {
			workType = new WorklogWorkType();
			workType.setName(name);
			workType = workTypeRepository.save(workType);
		}
		return workType;
	}

	public ServiceType findServiceByName(String name) {
		ServiceType entity = serviceTypeRepository.findFirstByName(name);
		if (entity == null) {
			entity = new ServiceType();
			entity.setName(name);
			entity = serviceTypeRepository.save(entity);
		}
		return entity;
	}

	
	@Override
	public List<ValidationDto> calculateAlarms(Integer employeeId, Instant fromDate, Instant toDate) {
		
		List<Worklog> worklogs = worklogRepository.findByEmployeeIdBetweenDates(employeeId, fromDate, toDate);
		
		List<ValidationDto> worklogsPerDay = separateWorklogsPerDay(worklogs);

		for (ValidationDto validation : worklogsPerDay) {
			validateWorklogs(validation);
		}

		return worklogsPerDay;
	}
	
	private void validateWorklogs(ValidationDto validation) {
		if (VOID_WORKLOG.equals(validation.getAlert())) {
			validation.setAlert(validation.getAlert() + " ["+validation.getWorklogs().size()+"]");
			return;
		}
		Duration totalWorkTimePerDay = Duration.ZERO;
		Instant startWorking = null;
		List<Worklog> worklogs = validation.getWorklogs();
		DayOfWeek dayOfWeek = validation.getDate().getDayOfWeek();
		boolean start = true;
		Worklog prevLog = null;
		for (Worklog currLog : worklogs) {
			Instant currLogDate = currLog.getDate();
			if (workable(currLog)) {
				if (start) {
					int hour = currLogDate.atZone(ZoneOffset.UTC).getHour();
					if (dayOfWeek.getValue() < DayOfWeek.FRIDAY.getValue() && hour < 8) {
						validation.addAlert("Este dia se ha entrado antes de la hora minima");
					} else if (dayOfWeek == DayOfWeek.FRIDAY && hour < 7) {
						validation.addAlert("Este dia se ha entrado antes de la hora minima");
					}
				}
				start = false;
				if (startWorking == null) {
					startWorking = currLogDate;
				}
			} else {
				if (start) {
					start = false;
					// No se ficho al entrar, considero el inicio del dia
					startWorking = currLogDate.truncatedTo( ChronoUnit.DAYS );
					validation.addAlert(MISSING_FIRST_START);
				}
				if (startWorking == null) {
					if (prevLog != null && REST.equals(prevLog.getType().getName())) {
						validation.addAlert(MISSING_REST + "[ "+currLogDate.atZone(ZoneOffset.UTC).getHour()+"h ]");	
					} else {
						validation.addAlert(MISSING_WORK + "[ "+currLogDate.atZone(ZoneOffset.UTC).getHour()+"h ]");
					}
				} else {
					// Sumo desde la ultima hora que se trabajó hasta ahora
					Instant endWorking = currLogDate;
					Duration totalWorkTime = Duration.between(startWorking, endWorking);
					totalWorkTimePerDay = totalWorkTimePerDay.plus(totalWorkTime);	
				}
				startWorking = null;
			}
			prevLog = currLog;
		}
		
		if (startWorking != null) {
			// No se fichó al terminar
			validation.addAlert(MISSING_END);
			// Sumo desde la ultima hora que se trabajó hasta el final del dia
			Instant endWorking = Instant.from(validation.getDate());
			endWorking = endWorking.plus(23, ChronoUnit.HOURS).plus(59, ChronoUnit.MINUTES);
			Duration totalWorkTime = Duration.between(startWorking, endWorking);
			totalWorkTimePerDay = totalWorkTimePerDay.plus(totalWorkTime);
		}
		validation.setDuration(totalWorkTimePerDay);
		long hours = totalWorkTimePerDay.toHours();
		if (hours >= 10) {
			validation.addAlert("Este dia se han echado mas de 10 horas");
		} else if (hours < 7) {
			validation.addAlert("Este dia se han echado solo "+hours+" horas");
		}
	}

	private List<ValidationDto> separateWorklogsPerDay(List<Worklog> worklogs) {

		List<ValidationDto> worklogsPerDay = new ArrayList<ValidationDto>();
		int prevDay = -1;
		int currDay = -1;
		ValidationDto currValidation = new ValidationDto();
		worklogsPerDay.add(currValidation);
		ValidationDto voidWorklog = null;
		for (Worklog currLog : worklogs) {
			if (currLog.getDate() == null) {
				if (voidWorklog == null) {
					voidWorklog = new ValidationDto();
					voidWorklog.setAlert(VOID_WORKLOG);
					worklogsPerDay.add(voidWorklog);
				}
				voidWorklog.addWorklog(currLog);
				continue;
			}
			ZonedDateTime zdt = currLog.getDate().atZone(ZoneOffset.UTC);
			currDay = zdt.getDayOfMonth();
			if (prevDay == -1) {
				prevDay = currDay;
			}
			if (prevDay != currDay) {
				currValidation = new ValidationDto();
				worklogsPerDay.add(currValidation);
				prevDay = currDay;
			}
			currValidation.setDate(zdt);
			currValidation.addWorklog(currLog);
		}
		return worklogsPerDay;
	}
	
	private Boolean workable(Worklog currLog) {
		// Se trabaja sólo si se entra a trabajar o se sale del descanso
		if (IN.equals(currLog.getRecordType().getName()) && WORK.equals(currLog.getType().getName())) {
			return true;
		}
		if (OUT.equals(currLog.getRecordType().getName()) && REST.equals(currLog.getType().getName())) {
			return true;
		}
		// En cualquier otro caso no se trabaja
		return false;
	}

}
