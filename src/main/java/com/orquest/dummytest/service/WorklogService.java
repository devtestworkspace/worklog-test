package com.orquest.dummytest.service;

import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.List;

import com.orquest.dummytest.model.dto.ValidationDto;
import com.orquest.dummytest.model.dto.WorklogDto;

public interface WorklogService {

	List<WorklogDto> findByFilter(WorklogDto filter);
	
	List<WorklogDto> findByEmployeeId(Integer employeeId);

	List<WorklogDto> create(List<WorklogDto> worklogDto) throws IllegalAccessException, InvocationTargetException;

	void delete(Long id);

	void update(Long id, WorklogDto updated);

	List<ValidationDto> calculateAlarms(Integer employeeId, Instant fromDate, Instant toDate);

}
