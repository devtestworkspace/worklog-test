package com.orquest.dummytest.controller;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orquest.dummytest.model.dto.ValidationDto;
import com.orquest.dummytest.model.dto.WorklogDto;
import com.orquest.dummytest.model.dto.WorklogDtoRaw;
import com.orquest.dummytest.service.WorklogService;

import io.swagger.annotations.ApiParam;

@CrossOrigin
@RestController
@RequestMapping("/fichajes")
public class WorklogController {

	@Autowired
	WorklogService worklogService;

	@Autowired
	protected Logger logger;

	@GetMapping("")
	public ResponseEntity<List<WorklogDto>> findByFilter(WorklogDtoRaw filter) {
		try {

			logger.info("findByFilter " + filter);

			List<WorklogDto> worklogs = worklogService.findByFilter(filter);

			return new ResponseEntity<>(worklogs, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("findByFilter " + filter, e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("")
	public ResponseEntity<List<WorklogDto>> create(@RequestBody List<WorklogDto> worklogs) {
		logger.info("create " + worklogs);
		try {
			List<WorklogDto> worklogCreated = worklogService.create(worklogs);
			return new ResponseEntity<>(worklogCreated, HttpStatus.CREATED);
		} catch (Exception e) {
			logger.error("creating " + worklogs, e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<HttpStatus> update(@PathVariable("id") Long id, @RequestBody WorklogDto updated) {

		if (id == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		try {
			worklogService.update(id, updated);
		} catch (Exception e) {
			logger.error("updating [" + id + "] ", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.OK);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> delete(@PathVariable("id") Long id) {
		if (id == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		try {
			worklogService.delete(id);

		} catch (Exception e) {
			logger.error("deleting [" + id + "] ", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/validate/{employeeId}")
	public ResponseEntity<List<ValidationDto>> calculateAlarms(@PathVariable("employeeId") Integer employeeId,
			@ApiParam(value = "Format 'yyyy-MM-dd'") @RequestParam(name = "fromDate", required = false) String fromDate,
			@ApiParam(value = "Format 'yyyy-MM-dd'") @RequestParam(name = "toDate", required = false) String toDate) {
		try {

			logger.info("calculateAlarms " + employeeId + ", " + fromDate + ", " + toDate);
			Instant paramFromDate = parseDate(fromDate);
			Instant paramToDate = parseDate(toDate);
			List<ValidationDto> alarms = worklogService.calculateAlarms(employeeId, paramFromDate, paramToDate);

			return new ResponseEntity<>(alarms, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("calculateAlarms " + employeeId, e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private Instant parseDate(String fromDate) {
		if (fromDate != null) {
			
			try {
				return LocalDate.parse(fromDate, DateTimeFormatter.ISO_DATE).atStartOfDay(ZoneOffset.UTC).toInstant();
			} catch (Exception e) {
				logger.error("calculateAlarms unable to parse " + fromDate, e);
			}
		}
		return null;
	}
}
