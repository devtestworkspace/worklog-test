package com.orquest.dummytest;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.orquest.dummytest.model.dto.ValidationDto;
import com.orquest.dummytest.service.WorklogService;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
class WorklokValidationTests {

	@Autowired
	private WorklogService worklogService;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;


	private String getApiUrl() {
		return "http://localhost:" + port + "/api/fichajes";
	}
	
	@BeforeAll
	void loadData() throws IOException, URISyntaxException {
		ClassLoader loader = ClassLoader.getSystemClassLoader();

		String json = Files.lines(Paths.get(loader.getResource("fichero_3.json").toURI())).parallel()
				.collect(Collectors.joining());

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(json, headers);
		this.restTemplate.postForObject(getApiUrl(), request, List.class);

	}

	@Test
	@Order(1)
	void normalDayValidations() {
		
		List<ValidationDto> validations = worklogService.calculateAlarms(123456789, null, null);
		
		List<String> validationsStr = validations.stream()
                .map(validation -> validation.getDate() + ", " + validation.getDuration() + " -> " + (validation.getAlert() != null ? validation.getAlert() : " OK"))
                .collect(Collectors.toList());
		
		validationsStr.forEach(validation -> {
			System.out.println("validation " + validation);
		});
		
		assertThat(validations.size()).isEqualTo(5);
		assertThat(validationsStr.get(0)).containsIgnoringCase("Ok");
		assertThat(validationsStr.get(1)).containsIgnoringCase("antes de");
		assertThat(validationsStr.get(2)).containsIgnoringCase("mas de");
		assertThat(validationsStr.get(3)).containsIgnoringCase("Falta el registro");
		assertThat(validationsStr.get(4)).containsIgnoringCase("solo");
		
	}

	
}
