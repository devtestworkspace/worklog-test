package com.orquest.dummytest;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.orquest.dummytest.model.dto.ValidationDto;
import com.orquest.dummytest.model.dto.WorklogDto;
import com.orquest.dummytest.model.entity.Worklog;
import com.orquest.dummytest.model.entity.WorklogRecordType;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
class SpringBootJpaH2ApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private ModelMapper modelMapper;

	private String getApiUrl() {
		return "http://localhost:" + port + "/api/fichajes";
	}
	
	@Test
	@Order(1)
	void contextLoads() {

	}

	@Test
	@Order(2)
	void conversion() {
		Instant currTime = Instant.now();
		Worklog wl = new Worklog();
		wl.setDate(currTime);
		wl.setRecordType(new WorklogRecordType());
		wl.getRecordType().setName("name");
		WorklogDto result = modelMapper.map(wl, WorklogDto.class);
		assertThat(result.getRecordType()).isEqualTo("name");
		assertThat(result.getDate()).isEqualTo(currTime.toString());
	}

	@Test
	@Order(3)
	void createFile1() throws IOException, URISyntaxException {
		ClassLoader loader = ClassLoader.getSystemClassLoader();

		String json = Files.lines(Paths.get(loader.getResource("fichero_1.json").toURI())).parallel()
				.collect(Collectors.joining());
		System.out.println("Sending json for POST");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(json, headers);
		assertThat(this.restTemplate.postForObject(getApiUrl(), request, List.class))
				.isNotNull();
		System.out.println("Sent POST Ok");

		List fichajes = this.restTemplate.getForObject(getApiUrl(), List.class);

		assertThat(fichajes).isNotNull();
		assertThat(fichajes.size()).isEqualTo(396);
	}



	@Test
	@Order(4)
	void findAndDeleteWorklog() throws IOException, URISyntaxException {

		
		ParameterizedTypeReference<List<WorklogDto>> typeRef = new ParameterizedTypeReference<List<WorklogDto>>() {
		};
		ResponseEntity<List<WorklogDto>> responseEntity = restTemplate.exchange(getApiUrl()+"?id=1", HttpMethod.GET, new HttpEntity<>(null), typeRef);
		List<WorklogDto> fichajes = responseEntity.getBody();
		
		assertThat(fichajes).isNotNull();
		assertThat(fichajes.size()).isEqualTo(1);
		WorklogDto fichaje = (WorklogDto) fichajes.get(0);

		this.restTemplate.delete(getApiUrl() + "/1");

		System.out.println("Sent DELETE Ok");

		fichajes = this.restTemplate.getForObject(getApiUrl(), List.class);

		assertThat(fichajes).isNotNull();
		assertThat(fichajes.size()).isEqualTo(395);

		assertThat(this.restTemplate.postForObject(getApiUrl(), fichaje, List.class))
				.isNotNull();

		fichajes = this.restTemplate.getForObject(getApiUrl(), List.class);

		assertThat(fichajes).isNotNull();
		assertThat(fichajes.size()).isEqualTo(396);
	}

	@Test
	@Order(5)
	void updateWorklog() throws IOException, URISyntaxException {

		ParameterizedTypeReference<List<WorklogDto>> typeRef = new ParameterizedTypeReference<List<WorklogDto>>() {
		};
		ResponseEntity<List<WorklogDto>> responseEntity = restTemplate.exchange(getApiUrl()+"?id=2", HttpMethod.GET, new HttpEntity<>(null), typeRef);
		List<WorklogDto> fichajes = responseEntity.getBody();

		assertThat(fichajes).isNotNull();
		assertThat(fichajes.size()).isEqualTo(1);
		WorklogDto fichaje = fichajes.get(0);

		fichaje.setRecordType("LALALA");
		fichaje.setServiceId("BUM");

		this.restTemplate.put( getApiUrl() + "/2", new HttpEntity<>(fichaje));

		System.out.println("Sent PUT Ok");

		fichajes = this.restTemplate.getForObject(getApiUrl(), List.class);

		assertThat(fichajes).isNotNull();
		assertThat(fichajes.size()).isEqualTo(396);
		
		responseEntity = restTemplate.exchange(getApiUrl()+"?id=2", HttpMethod.GET, new HttpEntity<>(null), typeRef);
		fichajes = responseEntity.getBody();
		fichaje = fichajes.get(0);
		
		assertThat(fichaje.getRecordType()).isEqualTo("LALALA");
		assertThat(fichaje.getServiceId()).isEqualTo("BUM");
	}
	
	@Test
	@Order(6)
	void createFile2() throws IOException, URISyntaxException {
		ClassLoader loader = ClassLoader.getSystemClassLoader();

		String json = Files.lines(Paths.get(loader.getResource("fichero_2.json").toURI())).parallel()
				.collect(Collectors.joining());
		System.out.println("Sending json for POST");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(json, headers);
		assertThat(this.restTemplate.postForObject(getApiUrl(), request, List.class))
				.isNotNull();
		System.out.println("Sent POST Ok");

		List fichajes = this.restTemplate.getForObject(getApiUrl(), List.class);

		assertThat(fichajes).isNotNull();
		assertThat(fichajes.size()).isEqualTo(887);
	}
	
	@Test
	@Order(7)
	void validations222222222() throws IOException, URISyntaxException {

		
		ParameterizedTypeReference<List<ValidationDto>> typeRef = new ParameterizedTypeReference<List<ValidationDto>>() {
		};
		ResponseEntity<List<ValidationDto>> responseEntity = restTemplate.exchange(getApiUrl()+"/validate/222222222", HttpMethod.GET, new HttpEntity<>(null), typeRef);
		List<ValidationDto> validations = responseEntity.getBody();
		
		validations.forEach(validation -> {
			if (validation.getAlert() != null) {
				System.out.println("validation " + validation.getDate() + ", " + validation.getDuration() + " -> " + validation.getAlert());
				validation.getWorklogs().forEach(worklog -> {
					System.out.println("\t\t " + worklog.getId() + ", " + worklog.getDate() + ", " + worklog.getType().getName() + ", " + worklog.getRecordType().getName());
				});
			}
			
		});
		
		assertThat(validations).isNotNull();
		assertThat(validations.size()).isEqualTo(51);
		
	}
	
	@Test
	@Order(8)
	void validations222222222withDates() throws IOException, URISyntaxException {

		
		ParameterizedTypeReference<List<ValidationDto>> typeRef = new ParameterizedTypeReference<List<ValidationDto>>() {
		};
		ResponseEntity<List<ValidationDto>> responseEntity = restTemplate.exchange(getApiUrl()+"/validate/222222222?fromDate=2018-01-01&toDate=2018-01-16", HttpMethod.GET, new HttpEntity<>(null), typeRef);
		List<ValidationDto> validations = responseEntity.getBody();
		
		validations.forEach(validation -> {
			System.out.println("validation " + validation.getDate() + ", " + validation.getDuration() + " -> " + validation.getAlert());
			validation.getWorklogs().forEach(worklog -> {
				System.out.println("\t\t " + worklog.getId() + ", " + worklog.getDate() + ", " + worklog.getType().getName() + ", " + worklog.getRecordType().getName());
			});
		});
		
		assertThat(validations).isNotNull();
		assertThat(validations.size()).isEqualTo(11);
		
	}
}
